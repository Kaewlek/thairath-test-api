const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const data = require('./mock/thai-rath.json');
// const dataIg = require('./mock/data.json');
const cors = require('cors');

require('dotenv').config();

const middlewares = require('./middlewares');
// const api = require('./api');

const app = express();

app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());

// app.get('/', (req, res) => {
//   // get data ig
//   const news = dataIg.graphql.user.edge_owner_to_timeline_media.edges;
//   res.json(
//     news.map((item) => {
//       return {
//         src: item.node.display_url,
//         caption: item.node.edge_media_to_caption.edges[0].node.text,
//         gallery: item.node.edge_sidecar_to_children
//           ? item.node.edge_sidecar_to_children.edges.map((item) => {
//               return { src: item.node.display_url };
//             })
//           : [],
//       };
//     })
//   );
// });

app.get('/page/:page/length/:length', (req, res) => {
  const { page, length } = req.params;
  const result = data.slice((page - 1) * length, page * length);
  res.json({
    count: data.length,
    record: result.length,
    result,
  });
});

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
